class Uno_Util

  require 'selenium-webdriver'
  require 'rubygems'


  ## Setting the capabilities and Driver for the whole class
  caps = Selenium::WebDriver::Remote::Capabilities.new
  caps['app'] = 'C:\Users\ldoliveira\Desktop\uno.application'
  $driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999", desired_capabilities: caps

  def login_uno(username,password)

        uno_Window = $driver.find_element(id: 'frmLogin')

        uno_Window.find_element(id: 'UsernameTextbox').clear
        uno_Window.send_keys(username)

        uno_Window.find_element(id: 'PasswordTextbox').clear
        uno_Window.send_keys(password)

        uno_Window.find_element(id: 'LoginButton').click

  end

  def search_merchants(app_id)

    uno_main = $driver.find_element(id: 'frmMain')

    uno_main.find_element(name: 'Merchants').click

    uno_main.find_element(id: 'txtAppID').send_keys(app_id)

    uno_main.find_element(id: 'btnSearch').click

    search_box = uno_main.find_element(id: 'SearchGrid')

    search_box.find_element(name: 'Table row 1')

    $driver.action.move_to(search_box).double_click.perform

    sleep 10

    uno_popup = $driver.find_element(id:'frmHotNotes')

    uno_popup.find_element(id: 'btnClose').click

    uno_popup = $driver.find_element(name: 'Uno')

    uno_popup.find_element(id: '2').click

    uno_main.find_element(id: 'UltraExpandableGroupBox1').click

  end

  def close_app

  end

  def close_uno

    uno_main = $driver.find_element(id: 'frmMain')

    uno_main.find_element(name: 'Close').click

    uno_popup = $driver.find_element(id: 'frmAddNewTicket')

    uno_popup.find_element(id: 'btnSave').click

    uno_popup = $driver.find_element(id: 'frmLogout')

    uno_popup.find_element(id: 'pbExit').click

  end

end

user = Uno_Util.new

user.login_uno('mbryant', '')

user.search_merchants('41212')

##user.close_uno
